import java.util.*;
import java.lang.*;
import java.io.*;

public class Sum_of_natural_numbers {
    public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc=new Scanner(System.in);
      int n = sc.nextInt();
      int sum =0;
      for(int i=0; i<=n; i++)sum+=i;
      System.out.print(sum);
	}
}
