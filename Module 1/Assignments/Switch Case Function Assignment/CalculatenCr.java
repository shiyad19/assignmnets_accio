import java.util.*;
import java.lang.*;
import java.io.*;

public class CalculatenCr {
    public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      long n = sc.nextInt();
      long r = sc.nextInt();
      long a = fact(n);
      long b = fact(n-r);
      long c = fact(r);
      long nCr = a/(b*c);
      System.out.print(nCr);
	}
    static long fact(long x)
    {
      long res = 1;
      for(int i=1; i<=x; i++){
        res *= i;
      }
      return res;
    }
}
