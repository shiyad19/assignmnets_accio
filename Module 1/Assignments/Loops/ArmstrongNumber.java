import java.util.*;
import java.lang.*;
import java.io.*;

public class ArmstrongNumber
{

    static boolean check(int n){
        int x=n;
        int ans=0;
      while(n>0){
        int ones=n%10;
        n=n/10;
        ans+=(ones*ones*ones);
      }
      return x==ans;
    }
	public static void main (String[] args) throws java.lang.Exception
  	{
      for(int i=1;i<=500;i++){
        if(check(i)){
          System.out.println(i);
        }
        }
	}
}