import java.util.*;
import java.lang.*;
import java.io.*;

public class Factorial_with_loop
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner (System.in);
        
        int n = sc.nextInt();
        long sum = 1;
        for(int i=1; i<=n; i++)
        {
            sum = sum*i;
        }
        
      System.out.println(sum);
	}
}