import java.util.*;
import java.lang.*;
import java.io.*;

public class Sorted_Insert_Position
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc=new Scanner(System.in);
      int n=sc.nextInt();
      int arr[]=new int[n];
      for(int i=0;i<n;i++){
        arr[i]=sc.nextInt();
        }
      int k=sc.nextInt(); 
        
      int ans=n;
       for(int i=0;i<n;i++){
          if(arr[i]>=k){
            ans=i;
            break;
          }
       }
        
      System.out.print(ans);
	}
}
