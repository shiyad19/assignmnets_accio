import java.util.*;
import java.lang.*;
import java.io.*;

public class _2nd_Largest_from_array
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
          a[i] = sc.nextInt();
      }
      int mx = 0;
      for(int i=0; i<n; i++){
        if(a[i] > a[mx])mx = i;
      }
      int mx2 = -1000000;
      for(int i=0; i<n; i++){
        if(mx2<a[i] && i!=mx)
          mx2 = a[i];
      }
      System.out.println(mx2);
      // System.out.println(a[mx]);
	}
}