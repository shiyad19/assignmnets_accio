import java.util.*;
import java.lang.*;
import java.io.*;

public class Angry_Professor
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int t = sc.nextInt();
      while(t-- > 0){
      int n = sc.nextInt();
      int k = sc.nextInt();
      int[] a = new int[n];
      int cnt = 0;
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
        if(a[i] <= 0)cnt++;
      }
      if(cnt >= k)System.out.println("NO");
      else System.out.println("YES");
        }
	}
}