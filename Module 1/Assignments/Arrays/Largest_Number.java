import java.util.*;
import java.lang.*;
import java.io.*;

public class Largest_Number {

  public static void main (String[] args)
        {
          Scanner sc = new Scanner(System.in);
          int n = sc.nextInt();
          int a[] =new int[n];
          String[] str = new String[n];
          for(int i=0; i<n; i++){
            a[i] = sc.nextInt();
            str[i] = String.valueOf(a[i]);
          }
          Arrays.sort(str, new Comparator<String>(){
            public int compare(String a, String b){
                return (a+b).compareTo(b+a)>0 ? -1:1;
            } 
          });
          StringBuilder ans = new StringBuilder();
          for(String s: str){
            ans.append(s);
          }
          while(ans.charAt(0)=='0' && ans.length()>1){
            ans.deleteCharAt(0);
          }
          System.out.print(ans);
        }
}