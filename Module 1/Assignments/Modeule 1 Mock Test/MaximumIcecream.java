import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int x = sc.nextInt();
      Arrays.sort(a);
      int sum = 0, ans = 0;
      for(int i=0; i<n; i++){
        sum +=a [i];
        if(sum > x){
          ans = i;
          break;
        }
      }
      if(sum <= x)ans = n;
      System.out.println(ans);
		//your code here
	}
}