import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    static int[] solve(int n, int[] a){
      int x = a[n-1], flag = 0;
      for(int i=n-2; i>=0; i--){
        if(a[i] <= x){
          a[i+1] = x;
          flag = 1;
          break;
        }else{
          a[i+1] = a[i];
        }
      }
      if(flag == 0)a[0] = x;
      return a;
    }
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int m = sc.nextInt();
      int[] a = new int[m];
      for(int i=0; i<m; i++){
        a[i] = sc.nextInt();
      }
      for(int i=1; i<m; i++){
        a = solve(i+1, a);
        for(int j=0; j<m; j++){
          System.out.print(a[j] + " ");
        }
        System.out.println();
      }
		//your code here
	}
}