import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    static int help(int[] a, int n){
      int ans = 0;
      for(int i=1; i<n; i++){
        for(int j=i-1; j>=0; j--){
          if(a[j] > a[j+1]){
            int temp = a[j];
            a[j] = a[j+1];
            a[j+1] = temp;
            ans++;
          }else break;
        }
      }
      return ans;
    }
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int d = sc.nextInt();
      while(d-- > 0){
        int n = sc.nextInt();
        int[] a = new int[n];
        for(int i=0; i<n; i++){
          a[i] = sc.nextInt();
        }
        System.out.println(help(a, n));
      }
		//your code here
	}
}