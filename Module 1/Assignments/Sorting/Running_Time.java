import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int ans = 0;
      for(int i=1; i<n; i++){
        for(int j=i-1; j>=0; j--){
          if(a[j] > a[j+1]){
            int temp = a[j];
            a[j] = a[j+1];
            a[j+1] = temp;
            ans++;
          }else break;
        }
      }
      System.out.println(ans);
		//your code here
	}
}