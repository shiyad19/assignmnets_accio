import java.util.*;
import java.lang.*;
import java.io.*;

public class Alternate_Manner_Matrix_Traversal
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt(), m = sc.nextInt();
      int[][] a = new int[n][m];
      for(int i=0; i<n; i++){
        for(int j=0; j<m; j++){
          a[i][j] = sc.nextInt();
        }
      }

      //traversal
      for(int i=0; i<n; i++){
        if(i%2==0){//even case L->R
          for(int j=0; j<m; j++)
            System.out.print(a[i][j]+" ");
        }else{            //odd case L<-R
          for(int j=m-1; j>=0; j--)
            System.out.print(a[i][j]+" ");
        }
        // System.out.println();
      }
	}
}