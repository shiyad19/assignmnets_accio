import java.util.*;
import java.lang.*;
import java.io.*;

public class Spirally_traversing_a_matrix
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int r = sc.nextInt(), c = sc.nextInt();
      int[][] a = new int[r][c];
      for(int i=0; i<r; i++){
        for(int j=0; j<c; j++){
          a[i][j] = sc.nextInt();
        }
      }
      int[] b = new int[r*c];
      int x=0, i=0, j=0, dir=0, up=0, down=r-1, left=0, right=c-1;
      while(x < r*c){
        while(i<=down && j<=right && j>=left && i>=up){
          b[x++] = a[i][j];
          if(dir == 0)j++;
          else if(dir == 1)i++;
          else if(dir == 2)j--;
          else if(dir == 3)i--;
        }
        if(dir == 0){up++; i++; j--;}
        else if(dir == 1){right--; j--; i--;}
        else if(dir == 2){down--; i--; j++;}
        else if(dir == 3){left++; j++; i++;}
        dir = (dir + 1)%4;
      }
      for(x = 0; x<r*c; x++){
        System.out.print(b[x] + " ");
      }
	}
}