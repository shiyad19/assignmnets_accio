import java.util.*;
import java.lang.*;
import java.io.*;

public class Alt_matrix_sum
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int[][] a = new int[n][n];
      int b=0, w=0;
      for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
          a[i][j] = sc.nextInt();
          if((i+j)%2 == 0) b += a[i][j];
          else w += a[i][j];
        }
      }
      System.out.println(b+"\n"+w);
	}
}