import java.util.*;
import java.lang.*;
import java.io.*;

public class Ptice
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      sc.nextLine();
      String s = sc.nextLine();

      String a = new String("ABC");
      String b = new String("BABC");
      String g = new String("CCAABB");
      int a_ind=0, b_ind=0, g_ind=0, a_cnt=0, b_cnt=0, g_cnt=0;
      for(int i=0; i<n; i++){
        if(s.charAt(i)==a.charAt(a_ind))a_cnt++;
        if(s.charAt(i)==b.charAt(b_ind))b_cnt++;
        if(s.charAt(i)==g.charAt(g_ind))g_cnt++;
        a_ind = (a_ind+1)%3;
        b_ind = (b_ind+1)%4;
        g_ind = (g_ind+1)%6;
      }
      int mx = Math.max(a_cnt, Math.max(b_cnt, g_cnt));
      System.out.print(mx + " ");
      if(mx == a_cnt)System.out.println("Adrian");
      if(mx == b_cnt)System.out.println("Bruno");
      if(mx == g_cnt)System.out.println("Goran");
	}
}