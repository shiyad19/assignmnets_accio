import java.util.*;
import java.lang.*;
import java.io.*;

public class Shuffle_String
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      sc.nextLine();
      String s = sc.nextLine();
      int[] a = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      char[] c = new char[n];
      for(int i=0; i<n; i++){
        c[a[i]] = s.charAt(i);
      }
      String ans = new String(c);
      System.out.println(ans);
	}
}