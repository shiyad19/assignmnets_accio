import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        int[] b = new int[n];
        
        for(int i=0;i<n;i++){
            a[i] = sc.nextInt();
            b[i] = -1;
        }

        Arrays.sort(a);

        for(int i=0; i<n; i++){

            int flag = 0;
            while(i+1<n && a[i]==a[i+1]){
                i++;
                flag=1;
            }
            if(flag==1)
                b[i]=a[i];
        }

        int cnt=0;
        for(int i=0;i<n;i++){

            if(b[i]!=-1){
                System.out.print(b[i] + " ");
                cnt=1;
            }
        }
        if(cnt==0)
            System.out.println(-1);


        //Alternate Way only applicable if A[i]<=10^6

        // Scanner sc = new Scanner(System.in);
        // int n = sc.nextInt();
        // int[] a = new int[n];
        // int[] b = new int[100005];
        // Arrays.fill(b,0);
        // // for(i=0; i<100005; i++)b[i]=0;

        // int cnt=0;
        // for(int i=0;i<n;i++){
        //     a[i] = sc.nextInt();

        //     b[a[i]]++;
        //     if(b[a[i]]>1) cnt++;
        // }

        // if(cnt>0){
        //     for(int i=0;i<n;i++){
        //         if(b[i]>1)
        //             System.out.print(b[i] + " ");
        //     }
        // }
        // if(cnt==0)
        //     System.out.println(-1);
    }
}