import java.util.*;
import java.lang.*;
import java.io.*;

public class COUNT_SQUARES
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int x = 0;
      for(int i=1; i*i<n;i++){
        x++;
      }
      System.out.println(x);
	}
}