//Rotate Array
//https://leetcode.com/problems/rotate-array/

class Solution {
    public:
    void rotate(vector<int> & nums, int k) {
        int cnt = 0, n = nums.size();
        if (n == 0 || k <= 0)return;
        // int start=0;
        // while(cnt<n)
        // {
        //     int curr=start;
        //     int prev=nums[curr];
        //     while(true)
        //     {
        //         int nxt=nums[(curr+k)%n];
        //         nums[(curr+k)%n]=prev;
        //         prev=nxt;
        //         curr=(curr+k)%n;
        //         cnt++;
        //         if(curr==start)break;
        //     }
        //     start++;
        // }

        reverse(nums.begin(), nums.begin() + ((n - k) % n + n) % n);
        reverse(nums.begin() + ((n - k) % n + n) % n, nums.end());
        reverse(nums.begin(), nums.end());
    }
}