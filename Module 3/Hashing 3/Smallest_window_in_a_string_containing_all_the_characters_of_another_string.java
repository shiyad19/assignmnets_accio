import java.io.*;
import java.util.*;
public class Main {
    public static void main(String args[]) {
      Scanner sc = new Scanner(System.in);
      String s = sc.next();
      String p = sc.next();
      int mp1[] = new int[26];
      int mp2[] = new int[26];
      if(s.length() < p.length()){
        System.out.println(-1);
        return;
      }
      for(int i=0; i<p.length(); i++){
        mp2[p.charAt(i)-'a']++;
        mp1[s.charAt(i)-'a']++;
      }
      if(check(mp1, mp2)){
        System.out.println(s.substring(0,p.length()));
        return;
      }
      int len = s.length()+1;
      String ans = "";
      int i=0, j=p.length()-1;
      while(i<j){
        while(!check(mp1, mp2) && j+1<s.length()){
          j++;
          mp1[s.charAt(j)-'a']++;
        }
        if(check(mp1, mp2) && j-i+1 < len){
          len = j-i+1;
          ans = s.substring(i, j+1);
        }
        mp1[s.charAt(i)-'a']--;
        i++;
      }
      if(len != s.length()+1){
        System.out.print(ans);
      }else System.out.println(-1);
    }
  static boolean check(int[] a, int[] b){
    for(int i=0; i<26; i++){
      if(a[i] < b[i])return false;
    }
    return true;
  }
}