import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      int mp[] = new int[n+1];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
        mp[a[i]]++;
      }
      for(int i=1; i<=n; i++){
        if(mp[i]>1)System.out.print(i + " ");
      }
      for(int i=1; i<=n; i++){
        if(mp[i]==0)System.out.println(i);
      }
	}
}