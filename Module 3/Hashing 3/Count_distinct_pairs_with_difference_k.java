import java.util.*;
import java.io.*;
public class Main {
    public static void main(String args[]) {
        HashMap<Integer, Integer> cnt = new HashMap<>();
        Scanner input = new Scanner(System.in);
        int n = input.nextInt(), k = input.nextInt();
        for(int i = 0; i < n; i++){
            int x = input.nextInt();
            int val = 0;
            if(cnt.containsKey(x))
                val = cnt.get(x);
            val++;
            cnt.put(x, val);
        }
        long ans = 0;
        if(k == 0){
            for(Map.Entry<Integer, Integer> entry : cnt.entrySet()){
                if(entry.getValue() >= 2)
                    ans++;
            }
        }else{
            for(Map.Entry<Integer, Integer> entry : cnt.entrySet()){
                if(cnt.containsKey(entry.getKey() - k)){
                    ans++;
                }
            }
        }
        System.out.println(ans);
    }
}