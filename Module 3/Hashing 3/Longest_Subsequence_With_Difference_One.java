import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      Map<Integer, Integer> mp = new HashMap<>();
      int ans = 0;
      for(int i=0; i<n; i++){
          int x = mp.getOrDefault(a[i] + 1, 0);
          int y = mp.getOrDefault(a[i] - 1, 0);
          mp.put(a[i], Math.max(x, y) + 1);
          ans = Math.max(ans, mp.get(a[i]));
      }
      System.out.println(ans);
		//your code here
	}
}