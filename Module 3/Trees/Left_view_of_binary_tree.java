import java.util.*;
import java.lang.*;
import java.io.*;

class Node{
    int val;
    Node left, right;
    Node(int val){
        this.val = val;
        left = null;
        right = null;
    }
}

class BST{
    Node root;
    BST(){
      root = null;
    }
    Node insert(Node node, int val){
        if(node == null){
            node = new Node(val);
            return node;
        }
        if(val < node.val){
            node.left = insert(node.left, val);
        }else{
            node.right = insert(node.right, val);
        }
        return node;
    }
}

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      BST tree = new BST();
      for(int i = 0; i < n; i++){
          tree.root = tree.insert(tree.root, sc.nextInt());
      }
      Queue<Node> q = new LinkedList<>();
      q.add(tree.root);
      while(!q.isEmpty()){
        int m = q.size();
        for(int i=0; i<m; i++){
          Node node = q.remove();
          if(i==0)System.out.print(node.val + " ");
          if(node.left != null)q.add(node.left);
          if(node.right != null)q.add(node.right);
        }
      }
	}
}






