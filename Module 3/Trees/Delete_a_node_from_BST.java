import java.util.*;
import java.io.*;

class Node{
    int val;
    Node left, right;
    Node(int val){
        this.val = val;
        left = null;
        right = null;
    }
}

class BST{
    Node root;
    BST(){
      root = null;
    }
    Node insert(Node node, int val){
        if(node == null){
            node = new Node(val);
            return node;
        }
        if(val < node.val){
            node.left = insert(node.left, val);
        }else{
            node.right = insert(node.right, val);
        }
        return node;
    }
    int minValue(Node node){
        int minv = node.val;
        while (node.left != null) {
            node = node.left;
            minv = node.val;
        }
        return minv;
    }
    // int maxValue(Node node){
    //       int maxv = node.val;
    //       while (node.right != null) {
    //           node = node.right;
    //           maxv = node.val;
    //       }
    //       return maxv;
    // }
    Node delete(Node node, int k){
        if(node == null)return node;
        else if(k < node.val){
            node.left = delete(node.left, k);
        }else if(k > node.val){
            node.right = delete(node.right, k);
        }else{
            if(node.left == null)return node.right;
            else if(node.right == null)return node.left;
            node.val = minValue(node.right);
            node.right = delete(node.right, node.val);

            // if(node.right == null)return node.left;
            // else if(node.left == null)return node.right;
            // node.val = maxValue(node.left);
            // node.left = delete(node.left, node.val);
        }
        return node;
    }
    void preOrderTraversal(Node node){
        if(node == null)
            return;
        System.out.print(node.val + " ");
        preOrderTraversal(node.left);
        preOrderTraversal(node.right);
    }
}

public class Main {
    public static void main(String args[]) {
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int k = sc.nextInt();
      BST tree = new BST();
      for(int i=0; i<n; i++){
          tree.root = tree.insert(tree.root, sc.nextInt());
      }
      tree.root = tree.delete(tree.root, k);
      tree.preOrderTraversal(tree.root);
    }
}



















