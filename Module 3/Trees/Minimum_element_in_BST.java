import java.util.*;
import java.lang.*;
import java.io.*;

class Node{
  int val;
  Node left, right;
  Node(int val){
    this.val = val;
    left = null;
    right = null;
  }
}

class BST{
  Node root;
  BST(){
    root = null;
  }
  Node insert(Node node, int val){
    if(node == null){
      node = new Node(val);
      return node;
    }else if(val < node.val){
      node.left = insert(node.left, val);
    }else{
      node.right = insert(node.right, val);
    }
    return node;
  }
  int minValue(Node node){
    if(node.left == null)return node.val;
    return minValue(node.left);
  }
}

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      BST tree = new BST();
      for(int i=0; i<n; i++){
        tree.root = tree.insert(tree.root, sc.nextInt());
      }
      int ans = tree.minValue(tree.root);
      System.out.println(ans);
	}
}