import java.util.*;
import java.lang.*;
import java.io.*;

class Node{
  int val;
  Node left;
  Node right;
  Node(int val){
    this.val = val;
    left = null;
    right = null;
  }
}

class BST{
  Node root;
  BST(){
    root = null;
  }
  Node insert(Node node, int val){
    if(node == null){
      node = new Node(val);
      return node;
    }else if(val < node.val){
      node.left = insert(node.left, val);
    }else if(val > node.val){
      node.right = insert(node.right, val);
    }
    return node;
  }
  Node findLCA(Node node, int p, int q){
    if(node == null)return null;
    if(node.val > p && node.val > q){
      return findLCA(node.left, p, q);
    }else if(node.val < p && node.val < q){
      return findLCA(node.right, p, q);
    }else return node;
  }
}

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int t = sc.nextInt();
      while(t-- > 0){
        int p = sc.nextInt(), q = sc.nextInt();
        sc.nextLine();
        String str=sc.nextLine();
        String[] str_arr = str.split(" ");
        int n = str_arr.length;
        BST tree = new BST();
        for(int i=0; i<n; i++)
        {
          int x = Integer.parseInt(str_arr[i]);
          if(x!=-1){
            tree.root = tree.insert(tree.root, x);
          }
        }
        if(p>q){
          int tmp = p;
          p = q;
          q = tmp;
        }
        Node ans = tree.findLCA(tree.root, p, q);
        System.out.println(ans.val);
      }
	}
}
