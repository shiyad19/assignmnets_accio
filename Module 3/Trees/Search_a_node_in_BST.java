import java.util.*;
import java.lang.*;
import java.io.*;

class Node{
  int val;
  Node left, right;
  Node(int val){
    this.val = val;
    left = null;
    right = null;
  }
}

class BST{
  Node root;
  BST(){
    root = null;
  }
  Node insert(Node node, int val){
    if(node == null){
      node = new Node(val);
      return node;
    }else if(val < node.val){
      node.left = insert(node.left, val);
    }else{
      node.right = insert(node.right, val);
    }
    return node;
  }
}

public class Main
{
    public static boolean find(Node node, int x){
        if(node == null)
            return false;
        if(node.val == x)return true;
        if(node.val > x)return find(node.left, x);
        else  return find(node.right, x);
    }
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int x = sc.nextInt();
      BST tree = new BST();
      for(int i = 0; i < n; i++){
          tree.root = tree.insert(tree.root, sc.nextInt());
      }
      boolean ans = find(tree.root, x);
      if(ans)System.out.print("YES\n");
      else System.out.print("NO\n");
	}
}