import java.util.*;
import java.lang.*;
import java.io.*;

class Node{
  int val;
  Node left, right;
  Node(int val){
    this.val = val;
    left = null;
    right = null;
  }
}

class BST{
  Node root;
  BST(){
    root = null;
  }
  Node insert(Node node, int val){
    if(node == null){
      node = new Node(val);
      return node;
    }else if(node.val == val){
      return node;
    }else if(node.val > val){
      node.left = insert(node.left, val);
      return node;
    }else {
      node.right = insert(node.right, val);
      return node;
    }
  }
}

public class Main
{
    public static void preOrderTraversal(Node node){
      if(node == null)return;
      System.out.print(node.val + " ");
      preOrderTraversal(node.left);
      preOrderTraversal(node.right);
    }
    
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int x = sc.nextInt();
      BST tree = new BST();
      for(int i=0; i<n; i++){
        tree.root = tree.insert(tree.root, sc.nextInt());
      }
      tree.root = tree.insert(tree.root, x);
      preOrderTraversal(tree.root);
	}
}









