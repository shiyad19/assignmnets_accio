import java.io.*;
import java.util.*;
public class Main {
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        int n= input.nextInt();
        HashMap<String, Integer> map = new HashMap<>();
        String[] s = new String[n];
        int max= 0;
        for(int i = 0; i < n; i++){
            s[i] = input.next();
            int cnt = 0;
            if(map.containsKey(s[i]))
                cnt = map.get(s[i]);
            cnt++;
            map.put(s[i], cnt);
            max = Math.max(max, cnt);
        }
        String ans = "";
        for(int i = 0; i < n; i++){
            if(map.get(s[i]) == max){
                map.put(s[i], 0);
                ans = s[i];
            }
        }
        System.out.println(ans);
    }
}