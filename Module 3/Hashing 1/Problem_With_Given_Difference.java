import java.util.*;
import java.lang.*;
import java.io.*;

public class Main {
	public static int solve(int[] A, int B) {
        HashSet<Integer> map = new HashSet<>();
        for(int i : A){
            if( map.contains(i-B) || map.contains(B+i))
                return 1;
            map.add(i);
        }
        return 0;
    }
  public static void main (String[] args)
	{
		Scanner sc = new Scanner(System.in);
	    
	    	int n = sc.nextInt();
			int[] A = new int[n];
	    	for(int i=0 ;i<n ;i++)
			{
				A[i] = sc.nextInt();
			}
			int B = sc.nextInt();
			System.out.println(solve(A,B));
	    	
		
	}
}