import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner sc = new Scanner(System.in);
		int[] x = new int[26], y = new int[26];
		for(int i=0; i<26; i++){
			x[i] = 0;
			y[i] = 0;
		}
		int n = sc.nextInt();
		String a = sc.next();
		n = a.length();
		int m = sc.nextInt();
		String b = sc.next();
		m = b.length();

		if(n < m) return;

		for(int i=0; i<m; i++){
			y[b.charAt(i)-'a']++;
			x[a.charAt(i)-'a']++;
		}
		if(check(x,y) == true) System.out.print(0 + " ");

		for(int i=m; i<n; i++){
			x[a.charAt(i)-'a']++;
			x[a.charAt(i-m)-'a']--;

			if(check(x,y) == true) System.out.print(i-m+1 + " ");
		}

	}
	static boolean check(int[] x, int[] y){
		for(int i=0; i<26; i++){
			if(x[i] != y[i])return false;
		}
		return true;
	}
}