import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int b = sc.nextInt();
      int i = 0, j = 0, sum = 0, ans = 0;
      while(j<n){
        sum += a[j];
        j++;
        if(sum<b){
          ans += j-i;
        }
        else{
          while(sum>=b && i<j){
            sum -= a[i];
            i++;
          }
          ans += j-i;
        }
      }
      System.out.println(ans);
		//your code here
	}
}