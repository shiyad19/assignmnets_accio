import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int i = 0, j = n-1, ans = 0;
      while(i < j){
        int w = j-i;
        int h = Math.min(a[i], a[j]);
        int area = w*h;
        ans = Math.max(ans, area);
        if(a[i] < a[j])i++;
        else j--;
      }
      System.out.println(ans);
		//your code here
	}
}