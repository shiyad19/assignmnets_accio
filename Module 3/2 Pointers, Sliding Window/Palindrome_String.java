import java.util.*;
import java.lang.*;
import java.io.*;

class Main {
    public static void main (String[] args)
	{
      Scanner sc = new Scanner(System.in);
	  String A = sc.nextLine();
      System.out.println(isPalindrome(A));
	}
    public static int isPalindrome(String A) {
      int front = 0;
      int rear = A.length()-1;
      while(front < rear){
          if(!Character.isLetterOrDigit(A.charAt(front))){
              front++;
              continue;
          }
          if(!Character.isLetterOrDigit(A.charAt(rear))){
              rear--;
              continue;
          }
          if(Character.toLowerCase(A.charAt(front)) != Character.toLowerCase(A.charAt(rear))){
              return 0;
          }
          front++;
          rear--;
      }
      return 1;
    }
}