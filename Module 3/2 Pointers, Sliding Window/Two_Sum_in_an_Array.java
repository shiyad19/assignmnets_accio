import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int t = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      Map<Integer, Integer> mp = new HashMap<>();
      for(int i=0; i<n; i++){
        if(mp.containsKey(t-a[i])){
          System.out.println(mp.get(t-a[i]) + " " + i);
          return;
        }
        mp.put(a[i], i);
      }
		//your code here
	}
}