import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int x = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int sum = 0, ans = (int)1e9;
      int i = 0, j = 0;
      while(j < n){
        sum += a[j];
        while(sum > x && i<=j){
          ans = Math.min(ans, j-i+1);
          sum -= a[i];
          i++;
        }
        j++;
      }
      if(ans == (int)1e9)ans = 0;
      System.out.println(ans);
		//your code here
	}
}