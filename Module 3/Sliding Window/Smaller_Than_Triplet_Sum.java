import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
      }
      int t = sc.nextInt();
      Arrays.sort(a);
      int cnt = 0;
      for(int i=0; i<n; i++){
        for(int j=i+1; j<n; j++){
          int x = check(a, n, t-a[i]-a[j]);
          if(a[i] < t-a[i]-a[j])x--;
          if(a[j] < t-a[i]-a[j])x--;
          cnt += x;
        }
      }
      System.out.println(cnt/3);
		//your code here
	}
    static int check(int[] a, int n, int p){
      int l = 0, r = n-1;
      int ans = -1;
      while(l<=r){
        int mid = (l+r)/2;
        if(a[mid] < p){
          ans = mid;
          l = mid+1;
        }else r=mid-1;
      }
      return ans+1;
    }
}




// import java.util.*;

// public class Main {
//     public static void main(String[] args) throws Throwable {
//         Scanner sc = new Scanner(System.in);
//         int n = sc.nextInt();
//         int[] A = new int[n];
//         for(int i=0;i<n;i++) {
//             A[i] = sc.nextInt();
//         }
//         int target = sc.nextInt();
//         int ans = Solution.threeSumSmaller(n,A,target);
//         System.out.println(ans);
//     }
// }
// class Solution {
//     static int threeSumSmaller(int n, int[] arr, int target) {
//         Arrays.sort(arr);
//         int count = 0;
//         for (int i = 0; i < n; i++) {
//             int st = i + 1, en = n - 1;
//             int updTarget = target - arr[i];
//             while (st < en) {
//                 while (en > 0 && arr[st] + arr[en] >= updTarget) {
//                     en--;
//                 }
//                 count += Math.max(0, en - st);
//                 st++;
//             }
//         }
//         return count;
//     }
// }