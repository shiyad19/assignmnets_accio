import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      int pre[] = new int[n+1];
      HashMap<Integer, Integer> mp = new HashMap<>();
      mp.put(0, 1);
      String ans = "NO";
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
        pre[i+1] = pre[i] + a[i];
        if(mp.containsKey(pre[i+1]))ans = "YES";
        else mp.put(pre[i+1], 1);
      }
      System.out.println(ans);
		//your code here
	}
}