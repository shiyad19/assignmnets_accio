import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
	public static void main (String[] args) throws java.lang.Exception
	{
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int a[] = new int[n];
      int b[] = new int[n];
      HashMap<Integer, Integer> mp = new HashMap<>();
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
        b[i] = a[i];
        mp.put(b[i], i);
      }
      Arrays.sort(b);
      int ans = 0;
      HashMap<Integer, Integer> vis = new HashMap<>();
      for(int i=0; i<n; i++){ 
        if(a[i] != b[i] && !vis.containsKey(b[i])){
          int cnt = 0;
          int j = i;
          while(!vis.containsKey(j)){
            vis.put(j, 1);
            cnt++;
            j = mp.get(b[j]);
          }
          ans += cnt>0 ? cnt-1:0;
        }
      }
      System.out.println(ans);
		//your code here
	}
}