import java.util.*;
import java.lang.*;
import java.io.*;

public class Main
{
    public static void main (String[] args) throws java.lang.Exception
	{
		//your code here
      Scanner sc = new Scanner(System.in);
      int n = sc.nextInt();
      int k = sc.nextInt();
      int a[] = new int[n];
      int mp[] = new int[100001];
      for(int i=0; i<n; i++){
        a[i] = sc.nextInt();
        a[i] = a[i] % k;
        mp[a[i]]++;
      }
      int ans = 0;
      for(int i=1; i<=k/2; i++){
        if(k-i == i){
          ans += (mp[i]*(mp[k-i]-1))/2;
        }else{
          ans += mp[i]*mp[k-i];
        }
      }
      ans += (mp[0]*(mp[0]-1))/2;
      System.out.println(ans);
	}
}